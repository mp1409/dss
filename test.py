""" Tests for dss. """

import unittest
from unittest import mock

import dss


class DSSTest(unittest.TestCase):
    RAW_INPUT = (
        'tank/home\t6039196574104\n'
        'tank/home/alpha\t7276468056\n'
        'tank/home/beta\t78451627584\n'
        'tank/home/gamma\t244079616\n'
        'tank/home/delta\t297357054984\n'
        'tank/home/epsilon\t4250597736\n'
    )

    PARSED_LIST = [
        ('tank/home/delta', 297357054984),
        ('tank/home/beta', 78451627584),
        ('tank/home/alpha', 7276468056),
        ('tank/home/epsilon', 4250597736),
        ('tank/home/gamma', 244079616)
    ]

    def test_human_readable_bytes(self):
        self.assertEqual(dss.human_readable_bytes(0), '0 B')
        self.assertEqual(dss.human_readable_bytes(1), '1 B')
        self.assertEqual(dss.human_readable_bytes(1023), '1023 B')
        self.assertEqual(dss.human_readable_bytes(1024), '1 KiB')
        self.assertEqual(dss.human_readable_bytes(1536), '1.5 KiB')
        self.assertEqual(dss.human_readable_bytes(137438953472), '128 GiB')
        self.assertEqual(dss.human_readable_bytes(3693645496320), '3.36 TiB')

    def test_read_list(self):
        completed_process = mock.Mock(stdout=self.RAW_INPUT.encode())

        with mock.patch('subprocess.run', return_value=completed_process):
            self.assertEqual(dss.read_list('tank/home'), self.RAW_INPUT)

    def test_parse_list(self):
        self.assertEqual(
            dss.parse_list('tank/home', self.RAW_INPUT), self.PARSED_LIST
        )

    def test_print_list(self):
        print_list = [
            'tank/home/delta   276.94 GiB',
            'tank/home/beta     73.06 GiB',
            'tank/home/alpha     6.78 GiB',
            'tank/home/epsilon   3.96 GiB',
            'tank/home/gamma   232.77 MiB'
        ]

        with mock.patch('builtins.print') as p:
            dss.print_list(self.PARSED_LIST)
            calls = [call[0][0] for call in p.call_args_list]
            self.assertEqual(calls, print_list)
