#!/usr/bin/env python3
""" List the most storage-intensive ZFS datasets. """

import argparse
import operator
import subprocess


def human_readable_bytes(number):
    """Convert a number of bytes to a human readable string."""
    prefix = ''

    for p in ['Ki', 'Mi', 'Gi', 'Ti', 'Pi']:
        if number < 1024:
            break
        else:
            prefix = p
            number /= 1024

    numberstr = '{:.2f}'.format(float(number)).rstrip('0').rstrip('.')
    return '{} {}B'.format(numberstr, prefix)


def read_list(parent):
    """ Read the list of datasets. """
    process = subprocess.run(
        ('/sbin/zfs', 'list', '-H', '-p', '-o', 'name,used', '-r', parent),
        stdout=subprocess.PIPE, check=True
    )

    return process.stdout.decode()


def parse_list(parent, text):
    """ Parse and sort the list of datasets. """
    result_list = []

    for line in text.splitlines():
        name, size = line.split()

        if name != parent:
            result_list.append((name, int(size)))

    result_list.sort(key=operator.itemgetter(1), reverse=True)
    return result_list


def print_list(result_list):
    """ Print the sorted lists of datasets. """
    output_table = []

    for name, size in result_list:
        output_table.append((name, human_readable_bytes(size)))

    names_len = max(len(line[0]) for line in output_table)
    sizes_len = max(len(line[1]) for line in output_table)
    line_len = names_len + 1 + sizes_len

    for name, size in output_table:
        fill = ' ' * (line_len - len(name) - len(size))
        print('{}{}{}'.format(name, fill, size))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='List the most storage-intensive ZFS datasets.'
    )
    parser.add_argument(
        'parent', help='only consider children of this dataset'
    )

    args = parser.parse_args()

    raw_list = read_list(args.parent)
    results = parse_list(args.parent, raw_list)
    print_list(results)
