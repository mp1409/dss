# dss.py [![build status](https://gitlab.com/mp1409/dss/badges/master/build.svg)](https://gitlab.com/mp1409/dss/commits/master)
dss.py is a short script to list the most storage-intensive ZFS datasets.

## Usage
```
usage: dss.py [-h] parent

List the most storage-intensive ZFS datasets.

positional arguments:
  parent      only consider children of this dataset

optional arguments:
  -h, --help  show this help message and exit
```